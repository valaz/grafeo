# Grafeo

https://grafeo.pro/

## Backend:
* Maven
* Spring-boot
* Spring Data (Hibernate)
* Spring Security
* Postgresql

## Frontend:
* npm
* react
* react-router
* react-intl
* material-ui
* recharts
* moment.js

## CI/CD
* Travis-ci
* Sonarcloud
* Dokku